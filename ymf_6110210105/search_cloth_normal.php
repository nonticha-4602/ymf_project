<!DOCTYPE html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="main_style.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>ค้นหาสินค้า</title>


</head>


<body style = "background-color:AliceBlue;">
    <?php include "navbar_normal.php"; ?>
    <div class="container">
        <?php
        include 'connect.php';
        session_start();    
        error_reporting(E_ERROR | E_PARSE);
        ?>
        <center>
            <br>
            <h1>ค้นหาสินค้า</h1>
            <form action="" method="GET" name="">
                <input type="text" name="k" value="<?php echo isset($_GET['k']) ? $_GET['k'] : ''; ?>"
                                placeholder="Enter your search keywords" />
                        <input class = "btn btn-primary btn-sm" type="submit" name="search" value="Search" />
            </form>
            <?php

            $k = isset($_GET['k']) ? $_GET['k'] : '';

            // create the base variables for building the search query
            $search_string = "SELECT * FROM cloth WHERE ";
            $display_words = "";

            // format each of search keywords into the db query to be run
            $keywords = explode(' ', $k);
            foreach ($keywords as $word) {
                $search_string .= "name LIKE '%" . $word . "%' OR ";
                $display_words .= $word . ' ';
            }
            $search_string = substr($search_string, 0, strlen($search_string) - 4);
            $display_words = substr($display_words, 0, strlen($display_words) - 1);
            $query = mysqli_query($conn, $search_string);
            $result_count = mysqli_num_rows($query);

            // display a message to the user to display the keywords

            ?>
            <?php


            ?>

            <?php
            if ($result_count > 0 and isset($_GET['search']) and $k != '') {
                echo 'ผลลัพธ์ที่ได้จากการค้นหา <i>"' . $display_words . '"</i>';
                echo '<div class="right">พบสินค้า <b><u>' . number_format($result_count) . '</u></b> รายการ</div><hr />';
                
                // display the header for the display table
                echo '<table class="search">';
                
                // loop though each of the results from the database and display them to the user
                while ($row = mysqli_fetch_array($query)) {
                    echo
                    '<div class="card" style="width: 20rem;">
                        <img src="picture/'.$row['cloth_image'].'" class="card-img-top" alt="...">
                        <div class="card-body">
                        <h5 class="card-title">ชื่อสินค้า ' . $row['name'] . '</h5>
                        <p class="card-text">ราคา ' . $row['price'] . ' บาท</p>
                        <p class="card-text">แบรนด์ ' . $row['brand'] . '</p>
                    </div>
        </div> <br>';
                }
                echo $row['cloth_image'];
                // end the display of the table
                echo '
    </table>';
            } else
                echo 'ไม่พบข้อมูลโปรดค้นหาใหม่';
            ?>
        </center>




    </div>



</body>


</html>