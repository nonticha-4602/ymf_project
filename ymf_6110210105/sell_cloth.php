<!DOCTYPE html>

<?php
session_start();

?>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="main_style.css">
	
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>สั่งซื้อสินค้า</title>
</head>

<body style="background-color:LightSlateGray;">
    <?php
    $id = $_GET['cloth_id'];
    $id_mem = $_SESSION['id'];
    // include "navbar.php";
    include 'connect.php';
    $select_script = "SELECT cloth_id,name,brand,details,price,size,type_name, cloth_image FROM cloth,entity WHERE cloth.type_id = entity.type_id and cloth_id =$id ";
    $query = mysqli_query($conn, $select_script);
    $res = mysqli_fetch_array($query);
    ?>
    <br>
    <br>
    <div class="container ">
        <div class="row">
            <div class="col">
                <center><br><br><br>
                    <h2>ซื้อสินค้า</h2><br><br>
                </center>
            </div>
	
        </div>
        <div class="row">
            <div class="col">
                <center>
                    <div class="text-center">
                        <img src="picture/<?php echo $res['cloth_image'];?>" style="width:200px;height:200px;" class="rounded float-start ">
                    </div>

                </center>

            </div>
            <div class="col">
                <h5>ชื่อสินค้า : <?php echo $res['name']; ?></h5>
                <p>แบรนด์ : <?php echo $res['brand']; ?></p>
                <p>รายละเอียด : <?php echo $res['details']; ?></p>
                <p>ไซส์ : <?php echo $res['size']; ?></p>

                <p>ประเภทสินค้า : <?php echo $res['type_name']; ?></p>
            </div>

        </div>
        <div class="row">
            <div class="col">
                <center><br><br>
                    <h3>ราคา <?php echo $res['price']; ?> บาท</h3>
                </center>

            </div>


        </div>
        <div class="row">
            <div class="col">
                <center>
                    <form method="post">
                        <button type="submit" class="btn btn-success" name='done'> ยืนยัน </button>
                    </form>


                </center>

            </div>
        </div>
        <?php
        if (isset($_POST['done'])) {
            
            $paymentsql = "insert into payment(cloth_id,mem_id,status) value($id,$id_mem,0)";
            $update_status = "update cloth set cloth_status = 1 where cloth_id = $id";
            mysqli_query($conn,$update_status);
            mysqli_query($conn,$paymentsql);
            header('location:upload_payment.php?cloth_id='.$id);
        }

        // }

        ?>
        <div class="row">
            <div class="col">
                <a href="index_member.php" class="btn btn-danger btn-sm">ย้อนกลับ</a>

            </div>

        </div>
    </div>









</body>

</html>