<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
	
	<link rel="stylesheet" href="main_style.css">
	<title>สรุปยอดการสั่งซื้อ</title>
</head>

<body class = "body">
	<?php
	include 'navbar_admin.php';
	?>
	<div class="container">
		<?php
		include 'connect.php';
		$sql_curday = "SELECT SUM(money) FROM payment WHERE YEAR(date)=YEAR(CURRENT_DATE()) and date=CURRENT_DATE() and status=1"; // get รายวัน
		$sql_query_day = mysqli_query($conn, $sql_curday);
		
		
		$sql_curm = "SELECT SUM(money) FROM payment WHERE YEAR(date)=YEAR(CURRENT_DATE()) and MONTH(DATE)=MONTH(CURRENT_DATE()) and status=1"; // รายเดือน
		$sql_query_month = mysqli_query($conn, $sql_curm);
		
		$sql_cury = "SELECT SUM(money) FROM payment WHERE YEAR(DATE)=YEAR(CURRENT_DATE()) and status=1";
		$sql_query_year = mysqli_query($conn,$sql_cury);
		
		$data_day = mysqli_fetch_array($sql_query_day);
		$data_month= mysqli_fetch_array($sql_query_month);
		$data_year = mysqli_fetch_array($sql_query_year);
		?>
		<br><br>
		<div class="row">
			<div class="col">

				<div class="card" style="width: 18rem;">
					<center>
						<div class="card-header">
							ยอดรายวัน
						</div>

						<div class="card-body">

							<p class="card-text"><?php echo ($data_day[0]); ?> บาท</p>

						</div>


					</center>

				</div>


			</div>
			<div class="col">

				<div class="card" style="width: 18rem;">
					<center>
						<div class="card-header">
							ยอดรายเดือน
						</div>

						<div class="card-body">

							<p class="card-text"><?php echo ($data_month[0]); ?> บาท</p>

						</div>


					</center>

				</div>


			</div>
			<div class="col">

				<div class="card" style="width: 18rem;">
					<center>
						<div class="card-header">
							ยอดรายปี
						</div>

						<div class="card-body">

							<p class="card-text"><?php echo ($data_year[0]); ?> บาท</p>

						</div>


					</center>

				</div>


			</div>


		</div>
		<div class="row">
			<div class="col">

				<!-- <canvas id="myChart" width="400" height="400"></canvas> -->
				<script>
					var ctx = document.getElementById('myChart').getContext('2d');
					var myChart = new Chart(ctx, {
						type: 'bar',
						data: {
							labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
							datasets: [{
								label: '# of Votes',
								data: [12, 19, 3, 5, 2, 3],
								backgroundColor: [
									'rgba(255, 99, 132, 0.2)',
									'rgba(54, 162, 235, 0.2)',
									'rgba(255, 206, 86, 0.2)',
									'rgba(75, 192, 192, 0.2)',
									'rgba(153, 102, 255, 0.2)',
									'rgba(255, 159, 64, 0.2)'
								],
								borderColor: [
									'rgba(255, 99, 132, 1)',
									'rgba(54, 162, 235, 1)',
									'rgba(255, 206, 86, 1)',
									'rgba(75, 192, 192, 1)',
									'rgba(153, 102, 255, 1)',
									'rgba(255, 159, 64, 1)'
								],
								borderWidth: 1
							}]
						},
						options: {
							scales: {
								yAxes: [{
									ticks: {
										beginAtZero: true
									}
								}]
							}
						}
					});
				</script>


			</div>





		</div>


		<!-- 
		<canvas id="myChart" width="400" height="400"></canvas>
		<script>
			var ctx = document.getElementById('myChart').getContext('2d');
			var myChart = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June','July','Aug','Sep','Oct','Nov','Dec'],
					datasets: [{
						label: '# of Votes',
						data: [12, 19, 3, 5, 2, 3,12,23,32,45,45,5],
						backgroundColor: [
							'rgba(255, 99, 132, 0.2)',
							'rgba(54, 162, 235, 0.2)',
							'rgba(255, 206, 86, 0.2)',
							'rgba(75, 192, 192, 0.2)',
							'rgba(153, 102, 255, 0.2)',
							'rgba(255, 159, 64, 0.2)'
						],
						borderColor: [
							'rgba(255, 99, 132, 1)',
							'rgba(54, 162, 235, 1)',
							'rgba(255, 206, 86, 1)',
							'rgba(75, 192, 192, 1)',
							'rgba(153, 102, 255, 1)',
							'rgba(255, 159, 64, 1)'
						],
						borderWidth: 1
					}]
				},
				options: {
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					}
				}
			});
		</script> -->
	</div>
</body>

</html>