<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>สมัครสมาชิก</title>

	<!-- Bootstrap -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="main_style.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet">
	

 </head>
  <body style = "background-color:Gold;"
	font-family: 'Kanit', sans-serif;
	>
<div class="container">
  <div class="row">
    <div class="col-md-4">
    <br><br><br><br><h3 align="center"><b>สมัครสมาชิก</b></h3> <br>
	
      <form action="" method="POST" name="register"  id="register">
       
          	<tr>
            <td width="50%" align="right"><b>Username</b> &nbsp;</td>
            <td width="31%"><input name="username" type="text" for="username"="username" class="form-control" placeholder="ชื่อผู้ใช้" required> <span id="msg2"></span> </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>	
		     <tr>
            <td align="right"><b>Name</b> &nbsp;</td>
            <td colspan="1">
            <input name="fname" type="text" for="fname"  class="form-control" placeholder="นาย/นาง/นางสาว + ชื่อ" required></td>
            <td>&nbsp;</td>
          </tr>
		  <tr>
            <td align="right"><b>Lastname</b> &nbsp;</td>
            <td colspan="1">
            <input name="lname" type="text" for="lname" size="50" class="form-control" placeholder="นามสกุล" required></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="right"><b>Password</b> &nbsp;</td>
            <td><input name="password" type="text" for="password" class="form-control" placeholder="รหัสผ่าน"  required></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="right"><b>Number phone &nbsp;</b> </td>
            <td><input name="phone" type="text" for="phone" class="form-control" placeholder="เช่น 0919999999" required></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
		  <tr>
            <td align="right"><b>Address &nbsp; </b></td>
            <td><input name="add" type="text" for="phone" class="form-control" placeholder="ที่อยู่" required></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td colspan="3" align="center">
            </td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td colspan="1" align="left">
			<p align="center">
			<input class="btn btn-danger"type="button" value='กลับหน้าหลัก' onClick="location='index.php'">&nbsp;&nbsp;
            <input class="btn btn-primary" type="submit" name="signup" value="สมัครสมาชิก">&nbsp;
            </td>
          </tr>
      </form>
	
      </div>
    </div>
  </div>
  
  <?php
        include 'connect.php';

        if (isset($_POST['signup'])) {
            $get_fname = $_POST['fname'];
            $get_lname = $_POST['lname'];
            $get_phone = $_POST['phone'];
            $get_user = $_POST['username'];
            $get_pass = $_POST['password'];
            $get_add = $_POST['add'];
            $status = 2;
            $id = 0;
            // รับ id กับ status
            
            // เพิ่มผู้ใช้
            $sql = "insert into member (fname, lname, phone, mem_status, username, password) 
            value ('$get_fname', '$get_lname', '$get_phone', $status, '$get_user', '$get_pass')";
           
            $query = mysqli_query($conn, $sql);
            
            $sql3 = "select mem_id, mem_status from member where username = '$get_user' and password ='$get_pass'";
            $query3 = mysqli_query($conn, $sql3);
            
            if ($temp = mysqli_fetch_array($query3)) {
                $status = $temp['mem_status'];
            }

            // เช็ค status
              if ($status == 2) {
                $id = $temp['mem_id'];

                // เพิ่มที่อยู่ผู้ใช้
                $sql2 = "insert into address (address,mem_id) value ('$get_add', '$id')";
                $query2 = mysqli_query($conn, $sql2);
                
                echo '<script type="text/javascript">';
                echo 'alert("สมัครสมาชิกสำเร็จ");';
                echo 'window.location.href = "login.php";';
                echo '</script>';

            ?>

        <?php
            } else {
              echo '<script type="text/javascript">';
                echo 'alert("สมัครสมาชิกผิดพลาด");';
                echo '</script>';
            }
            
        }
        if (isset($_POST['out'])) {
         header("Location:index.php");
        }
        ?>
      
<!--start footer-->      
<p align="center"> </p>
<!--end footer-->


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
	
  </body>
</html>