<!DOCTYPE html>
<html>

<head>
    <title>อัพเดตข้อมูล</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="main_style.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body style = "background-color:#d8e7fe;">

    <div class="container">
        <center>
        <h1 class="text-warning text-center"> Update Data </h1>

        <br>
        <br>
        <?php
        include "connect.php";
        $id = $_GET['id'];
        $sql = "select * from cloth,entity where cloth_id = '$id' and cloth.type_id = entity.type_id; ";
        $query = mysqli_query($conn, $sql);
        $res = mysqli_fetch_array($query);
        ?>
        <form method="POST">
            <table border="1">
                <tr>
                    <th>ID</th>
                    <td><?php echo $res['cloth_id']; ?> </td>
                </tr>

                <tr>
                    <th>Name</th>
                    <td><input type="text" name="bn" value="<?php echo $res['name']; ?>" required></td>
                </tr>
                <tr>
                    <th>Brand</th>
                    <td><input type="text" name="ba" value="<?php echo $res['brand']; ?>" required></td>
                </tr>
                <tr>
                    <th>Details</th>
                    <td><input type="text" name="bp" value="<?php echo $res['details']; ?>" required></td>
                </tr>
                <tr>
                    <th>Price</th>
                    <td><input type="text" name="bpr" value="<?php echo $res['price']; ?>" required></td>
                </tr>
                <tr>
                    <th>Size</th>
                    <td><input type="text" name="by" value="<?php echo $res['size']; ?>" required></td>
                </tr>
                <th>Type</th>
                <td>
                    <select name="cloth_type">
                        <option value="<?php echo $res['type_id']; ?>"> <?php echo $res['type_name']; ?> </option>
                        <option value="1">เสื้อยืด</option>
                        <option value="2">เสื้อเชิ้ต</option>
                        <option value="3">เสื้อฮูด</option>
                    </select>
                </td>
                </tr>
                </tr>
                <th>Image</th>
                <td>
                    <form action="upload.php" method="post" enctype="multipart/form-data">
                        <figure>
                            <img src="picture/<?php echo $res['cloth_image'];?>" class="img-thumbnail" alt="..." width="150"
                                height="150">
                        </figure>
                        <input type="file" id="fileToUpload" name="fileToUpload" accept="image/*">
                    </form>
                </td>
        </form>
        </td>
        </tr>

        </table>
        <br>
        <button class="btn" type="submit" name="done"> Submit </button>
        <button class="btn"><a href="index_admin.php">Display Page</a></button>
        </form>
        <?php
        if (isset($_POST['done'])) {
            $cloth_id = $res['cloth_id']; 
            $cloth_name = $_POST['bn'];
            $brand = $_POST['ba'];
            $details = $_POST['bp'];
            $price = $_POST['bpr'];
            $size = $_POST['by'];
            $type = $_POST['cloth_type'];
            if ($_POST['fileToUpload'] != "")
                $image = $_POST['fileToUpload'];
            else
                $image = $res['cloth_image'];

        $sql = "update cloth set name='$cloth_name', brand='$brand', details='$details', price='$price', size='$size',
        type_id = '$type', cloth_image = '$image' where cloth_id = '$cloth_id';";
        $query = mysqli_query($conn, $sql);
        header('location:index_admin.php');
        }

        ?>
        </center>
    </div>
    

</body>

</html>