<!DOCTYPE html>
<html>
    <meta charset="UTF-8">
    <head>
        <title>แจ้งชำระเงิน</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="main_style.css">
		
		<link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    </head>
    <body style = "background-color:#d8e7fe;">
        <?php
            include "navbar.php";

        ?>
        <div class = "container">

            <div class="d-flex justify-content-center">
                <br><br>
                <h1>แจ้งชำระเงิน</h1>
            </div>
            
            
            <?php
                session_start();
                include 'connect.php';
                $cloth_id = $_GET['cloth_id'];
                if($_SESSION['id']){
                    $id= $_SESSION['id'];
                    $name = "select * from payment where mem_id = $id";
                    $member = mysqli_query($conn, $name);
                    $mem_res = mysqli_fetch_array($member);
                }
                
                $price = mysqli_query($conn, "select price from cloth where cloth_id = $cloth_id");
                $price_cloth = mysqli_fetch_array($price);
            ?>

            <div class="d-flex justify-content-center">
                <form method="POST" enctype="multipart/form-data">
                    <label for="img">เพิ่มรูปภาพการโอนเงิน</label><br>
                    <input type="file" id="fileToUpload" name="fileToUpload" accept="image/*" required><br><br>
                    <label for="cradit" name="cradit">เลขที่บัญชี</label><br>
                    <input type="text" name="cradit" id="cradit" required><br>
                    <label for="date" name="date" placeholder="yyyy/mm/dd">วันเวลา</label><br>
                    <input type="text" name="date" id="date" placeholder="yyyy/mm/dd" required><br><br>
                    <label for="num_money" name="num_money">จำนวนเงิน : จำนวนเงินที่ต้องชำระ <?php echo $price_cloth['price'];?> </label><br>
                    <input type="text" name="num_money" id="num_money" required><br><br>
                    <select name="transport">
                        <option value="Thailane Post">Thailane Post</option>  
                        <option value="Kerry">Kerry</option>  
                        <option value="DHL">DHL</option>  
                    </select><br><br>
                    <input type="submit"  class="btn btn-primary" value="Submit", name="done">
                </form>
            </div>
            <?php
                error_reporting(E_ERROR | E_PARSE);
                $target_dir = "picture/";
                $picture_tmp = basename($_FILES["fileToUpload"]["name"]);
                $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
                $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
                if (isset($_POST['done'])) {
                    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                        // echo "comples";
                    }
                    $pointer = $picture_tmp;
                    $money = $_POST['num_money'];
                    $number = $_POST['cradit'];
                    $date = $_POST['date'];
                    $transport = $_POST['transport'];
                    $update_payment = "update payment set number = '$number', money = '$money', date = '$date', image = '$pointer', transport = '$transport' where cloth_id = $cloth_id";
                    // echo $update_payment;
                    $query = mysqli_query($conn, $update_payment);
                
                }
                
            ?>
            <a href="index_member.php"  class="btn btn-danger">กลับ</a>
        </div>
    </body>
</html>