<!DOCTYPE html>
<html>

<head>
     <link rel="preconnect" href="https://fonts.gstatic.com">
     <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="main_style.css">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>ตรวจสอบการชำระเงิน</title>
</head>

<body class = "body">
    <?php
    include "navbar_admin.php";
    ?>
    <div class="container">
	<center>
        <br><br><h1>ข้อมูลการชำระเงิน</h1>
        <div class="table">
        <table class="index">
            <tr>
                <th><center>ID</center></th>
                <th><center>รายการสั่งซื้อ</center></th>
                <th><center>ราคา</center></th>
                <th><center>ยอดที่จ่าย</center></th>
                <th><center>ใบเสร็จ</center></th>
                <th><center>สถานะ</center></th>
            </tr>
            <?php
                include 'connect.php';
                $call_sql = "select * from payment";
                $call_query = mysqli_query($conn, $call_sql);

                while ($res = mysqli_fetch_array($call_query)){
                    if($res['status']!=1){
                        $tmp = $res['cloth_id'];
                        $cloth_sql = "select * from cloth where cloth_id = $tmp;";
                        $cloth = mysqli_query($conn, $cloth_sql);
                        $res_cloth = mysqli_fetch_array($cloth);
                        ?>
                        <tr class="text-center">
                            <td> <?php echo $res['pay_id']; ?> </td>
                            <td> <?php echo $res_cloth['name']; ?> </td>
                            <td> <?php echo $res_cloth['price']; ?> </td>
                            <td> <?php echo $res['money']; ?> </td>
                            <td> <img src="picture/<?php echo $res['image'];?>"  style="width:120px;height:150px;"> </td>
                            <td><a class = "btn btn-primary" href="completed_payment.php?id=<?php echo $res['pay_id']; ?>" onclick="return confirm('จ่ายเงินสำเร็จ');"> สำเร็จ </a> 
                            <a class = "btn btn-danger" href="delete_payment.php?id=<?php echo $res['pay_id']; ?>" onclick="return confirm('จ่ายเงินไม่สำเร็จ');"> ไม่สำเร็จ </a></td>
                        
                        </tr>
                    <?php
                    }
                }
            ?>
        </table>
    </div>
    




</body>
    




</body>