-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 28, 2021 at 10:16 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clothstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `address` varchar(50) NOT NULL,
  `mem_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`address`, `mem_id`) VALUES
('นคร', 6),
('12313', 7),
('Suratthani', 8);

-- --------------------------------------------------------

--
-- Table structure for table `backet`
--

CREATE TABLE `backet` (
  `cloth_id` int(5) NOT NULL,
  `mem_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cloth`
--

CREATE TABLE `cloth` (
  `cloth_id` int(5) NOT NULL,
  `details` varchar(50) NOT NULL,
  `brand` varchar(20) NOT NULL,
  `name` varchar(40) NOT NULL,
  `type_id` int(5) NOT NULL,
  `size` varchar(4) NOT NULL,
  `price` varchar(10) NOT NULL,
  `cloth_image` varchar(50) NOT NULL,
  `cloth_status` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cloth`
--

INSERT INTO `cloth` (`cloth_id`, `details`, `brand`, `name`, `type_id`, `size`, `price`, `cloth_image`, `cloth_status`) VALUES
(1, 'สีขาว ลายการ์ตูน', 'H&M', 'เสื้อยืด', 1, 'M', '80', 'p1.jfif', 2),
(2, 'สีดำ ผ้าหนา', 'Aiiz', 'เสื้อวินเทจ', 3, 'L', '200', 'p2.jpg', 2),
(3, 'สีดำ', 'H&M', 'เสื้อยืด Oversize', 1, 'S', '80', 'p6.jpg', 1),
(4, 'สีขาว', 'Uniqlo', 'เสื้อเชิ้ตแขนยาว', 2, 'M', '250', 'p3.jpg', 1),
(5, 'สีขาว ลายคิตตี้', 'citty', 'เสื้อยืด', 1, 'S', '30', 'p5.jfif', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `entity`
--

CREATE TABLE `entity` (
  `type_id` int(5) NOT NULL,
  `type_name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `entity`
--

INSERT INTO `entity` (`type_id`, `type_name`) VALUES
(1, 'เสื้อยืด'),
(2, 'เสื้อเชิ้ต'),
(3, 'เสื้อฮูด'),
(5, 'เสื้อกีฬา');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `mem_id` int(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `fname` varchar(15) NOT NULL,
  `lname` varchar(15) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `mem_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`mem_id`, `username`, `password`, `fname`, `lname`, `phone`, `mem_status`) VALUES
(2, 'admin', '123', 'Admin', 'Testing', '0845345648', 1),
(6, 'member', '123', 'Member', 'Testing', '12345678', 2);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `cloth_id` int(10) NOT NULL,
  `status` int(2) NOT NULL,
  `money` int(10) NOT NULL,
  `number` varchar(10) NOT NULL,
  `date` date NOT NULL,
  `image` varchar(100) NOT NULL,
  `pay_id` int(10) NOT NULL,
  `mem_id` int(10) NOT NULL,
  `transport` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`cloth_id`, `status`, `money`, `number`, `date`, `image`, `pay_id`, `mem_id`, `transport`) VALUES
(4, 1, 250, '9324258795', '2564-02-28', 'pay.jpg', 57, 6, 'DHL'),
(1, 1, 80, '9324258795', '2564-02-28', 'pay.jpg', 58, 6, 'Kerry'),
(1, 1, 80, '9324258795', '2564-02-28', 'pay.jpg', 59, 6, 'Kerry'),
(1, 1, 80, '9324258795', '2564-02-28', 'pay.jpg', 61, 6, 'Kerry'),
(2, 1, 200, '9324258795', '2564-02-28', 'pay.jpg', 62, 6, 'DHL'),
(4, 0, 250, '9324258795', '2564-02-28', 'pay.jpg', 64, 6, 'DHL');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cloth`
--
ALTER TABLE `cloth`
  ADD PRIMARY KEY (`cloth_id`);

--
-- Indexes for table `entity`
--
ALTER TABLE `entity`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`mem_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`pay_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cloth`
--
ALTER TABLE `cloth`
  MODIFY `cloth_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;

--
-- AUTO_INCREMENT for table `entity`
--
ALTER TABLE `entity`
  MODIFY `type_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `mem_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `pay_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
