<!DOCTYPE html>
<html>

<head>
    
    <title>ร้านขายเสื้อผ้ามือสองออนไลน์</title>
    <style>
	body{background-color:#d8e7fe;}
	</style>
	<link rel="stylesheet" href="main_style.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet">
</head>

<body style = "background-color:#lavender;"
font-family: 'Kanit', sans-serif;
>
    <?php
    include "navbar_normal.php";
    error_reporting(E_ERROR | E_PARSE);

    ?>


    <div class="container" style = "text-align:center;">
        <br>
        <h1> ร้านเสื้อผ้ามือสองออนไลน์ </h1><br>
        <div>
            <?php
            include "connect.php";
            if($_SESSION['id']){
            $id= $_SESSION['id'];
            $name = "select * from member where mem_id = $id";
            $member = mysqli_query($conn, $name);
            $mem_res = mysqli_fetch_array($member);
            }
            ?>
        </div>



        <table class="table">
            <thead>
                <th scope="col"> สินค้า </th>
                <th scope="col"> แบรนด์ </th>
                <th scope="col"> รายละเอียด </th>
                <th scope="col"> ไซส์</th>
                <th scope="col"> ราคา </th>
            </thead>
            <tbody>

                <?php
                include 'connect.php';
                $select_script = "SELECT cloth_id,name,brand,details,price,size,type_name,cloth_image FROM cloth,entity WHERE cloth.type_id = entity.type_id and cloth_status = 0";
                $query = mysqli_query($conn, $select_script);

                while ($res = mysqli_fetch_array($query)) {



                ?>
                <tr>
                    <td class="align-middle">
                        <figure>
                            <img src="picture/<?php echo $res['cloth_image']; ?>" class="img-thumbnail" alt="..." width="100" height="100">

                            <figcaption class="figure-caption"><?php echo $res['name']; ?></figcaption>
                        </figure>

                    </td>
                    <td class="align-middle"><?php echo $res['brand']; ?></td>
                    <td class="align-middle"> <?php echo $res['details']; ?></td>
                    <td class="align-middle"><?php echo $res['size']; ?></td>
                    <td class="align-middle"><?php echo $res['price']; ?></td>

                </tr>

                <?php
                }
                ?>
            </tbody>
        </table>
        <br>
    </div>
</body>

</html>