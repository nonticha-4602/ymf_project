<!DOCTYPE html>
<html>

<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="main_style.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet">
		
    <title>จัดการสินค้า</title>
	
	<style>
	
	</style>
</head>
	<body style = "background-color:White;">
	  <center>
<body class="member">
    
    <?php
    session_start();    
    include "navbar_admin.php";

    ?>  
    <br>
    <h1 class="text-center"> ระบบจัดการข้อมูลสินค้า </h1>
    <div class="container">
        <?php
        include "connect.php";
        $id = $_SESSION['id'];
        ?>
        <h2>สวัสดี admin</h2>
     </center>

      <table border="1">
            <tr>
				<th><center>รหัสสินค้า</center></th>
                <th><center>ชื่อสินค้า </center></th>
                <th><center>แบรนด์ </center></th>
                <th><center>รายละเอียด</center></th>
                <th><center>ราคาสินค้า</center> </th>
                <th><center>ไซส์</center></th>
                <th><center>ประเภทสินค้า</center></th>
                <th><center>ภาพสินค้า</center></th>
                <th><center>Delete </center></th>
                <th><center>Update </center></th>
          
			</tr> 
            <?php
            include 'connect.php';
            $select_script = "SELECT cloth_id,name,brand,details,price,size,type_name,cloth_image FROM cloth,entity WHERE cloth.type_id = entity.type_id ";
            $query = mysqli_query($conn, $select_script);
            while ($res = mysqli_fetch_array($query)) {
            ?>

            <tr class="text-center">
                <td><?php echo $res['cloth_id']; ?></td>
                <td><?php echo $res['name']; ?> </td>
                <td><?php echo $res['brand']; ?> </td>
                <td><?php echo $res['details']; ?> </td>
                <td><?php echo $res['price']; ?> </td>
                <td><?php echo $res['size']; ?> </td>
                <td><?php echo $res['type_name']; ?> </td>
                <td>
                    <figure>
                        <img src="picture/<?php echo $res['cloth_image'];?>" class="img-thumbnail" alt="..." width="150"
                            height="150">
                    </figure>
                </td>
                <td> <button class="btn-danger btn"> <a href="delete_cloth.php?id=<?php echo $res['cloth_id']; ?>"
                            onclick="return confirm('Are you sure you want to delete ?');" class="text-white"> Delete </a>
                    </button> </td>
                <td> <button class="btn-primary btn"> <a href="update.php?id=<?php echo $res['cloth_id']; ?>"
                            class="text-white"> Update </a> </button> </td>
            </tr>
            <?php
            }
            ?>
        </table>
        <br>
        <div style="text-align: center;">
            <a href="insert_cloth.php"  class="btn btn-success">เพิ่มสินค้า</a> &nbsp;
            <a href="insert_typecloth.php" class="btn btn-info">เพิ่มประเภทสินค้า</a> &nbsp;
        </div>
    </div>
            


</body>

</html>