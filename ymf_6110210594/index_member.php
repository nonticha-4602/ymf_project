<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="main_style.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>ร้านเสื้อผ้ามือสองออนไลน์</title>

</head>

<body style = "background-color:White;">

    <?php
    session_start();    
    include "navbar.php";

    ?>


    <div class="container">
        <br>
        <h1 class="text-center"> ร้านเสื้อผ้ามือสองออนไลน์ </h1>
        <div>
            <?php
            include "connect.php";
            if($_SESSION['id']){
            $id= $_SESSION['id'];
            $name = "select * from member where mem_id = $id";
            $member = mysqli_query($conn, $name);
            $mem_res = mysqli_fetch_array($member);
            }
            ?>
            <h3>สวัสดีผู้ใช้ <?php echo ($mem_res['fname'] . " " . $mem_res['lname']); ?></h3>
        </div>



        <table class="table ">
            <thead>
			<center>
                <th scope="col"> ชื่อสินค้า</th>
                <th scope="col"> แบรนด์</th>
                <th scope="col"> รายละเอียด </th>
                <th scope="col"> ไซส์</th>
                <th scope="col"> ราคา </th>
                <th scope="col"></th>
			</center>
            </thead>
            <tbody>

                <?php
                include 'connect.php';
                $select_script = "SELECT cloth_id,name,brand,details,price,size,type_name,cloth_image FROM cloth,entity WHERE cloth.type_id = entity.type_id and cloth_status = 0";
                $query = mysqli_query($conn, $select_script);

                while ($res = mysqli_fetch_array($query)) {
                ?>
                    <tr>
                        <td class="align-middle">
                            <figure>
                                <img src="picture/<?php echo $res['cloth_image']; ?>" class="img-thumbnail"  width="150" height="150">
                                <figcaption class="figure-caption"><?php echo $res['name']; ?></figcaption>
                            </figure>

                        </td>
                        <td class="align-middle"><?php echo $res['brand']; ?></td>
                        <td class="align-middle"> <?php echo $res['details']; ?></td>
                        <td class="align-middle"><?php echo $res['size']; ?></td>
                        <td class="align-middle"><?php echo $res['price']; ?></td>
                        <td class="align-middle"><a href="sell_cloth.php?cloth_id=<?php echo $res['cloth_id']; ?>" class="btn btn-warning">ซื้อสินค้า</a></td>

                    </tr>

                <?php
                }
                ?>
            </tbody>
        </table>
        <br>
       
    </div>
</body>

</html>