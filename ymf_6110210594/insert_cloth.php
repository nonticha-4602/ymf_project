<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet">
    <title>เพิ่มสินค้า</title>
	
	<style>
	body{background-color:AliceBlue ;}
	</style>
</head>

<body>
    <center>
	<font color = "MediumBlue">
    <br><br><br><br><h1><b>เพิ่มสินค้า</b></h1>
    <br>
    <?php 
       include 'connect.php';
    ?>
    <form method="POST">
	<font color = "MediumBlue">
	
        <table>
            <tr>
                <th>รหัสสินค้า</th>
                <td><input type="text" name="cloth_id" value="" required></td>
            </tr>
            <tr>
                <th>ชื่อสินค้า</th>
                <td><input type="text" name="cloth_name" value="" required></td>
            </tr>
            <tr>
                <th>แบรนด์</th>
                <td><input type="text" name="cloth_brand" value="" required></td>
            </tr>
            <tr>
                <th>รายละเอียด</th>
                <td><input type="text" name="cloth_details" value=""></td>
                <!--onkeypress="return event.charcode >= 48 && event.charcode <= 57" maxlength="4"-->
            </tr>
            <tr>
                <th>ราคาสินค้า</th>
                <td><input type="text" name="cloth_price" value="" required></td>
            </tr>
            <tr>
                <th>ประเภทสินค้า</th>
                <td>
                    <select name="cloth_type">
                        <option value="">เลือก</option>
                        <option value="1">เสื้อยืด</option>
                        <option value="2">เสื้อเชิ้ต</option>
                        <option value="3">เสื้อฮูด</option>

                    </select>
                </td>
            </tr>
            <tr>
                <th>ไซส์</th>
                <td><input type="text" name="cloth_size" value="" require></td>
            </tr>
            <tr>
                <th>รูปภาพ</th>
                <form action="upload.php" method="post" enctype="multipart/form-data">
                    <td> <input type="file" id="fileToUpload" name="fileToUpload" accept="image/*" required></td>
                </form>
                <?php  
                error_reporting(E_ERROR | E_PARSE);
                   $target_dir = "picture/";
                   $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
                   $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
                   $uploadOk = 1;
                    if(isset($_POST["submit"])) {
                        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                        if($check !== false) {
                            echo "File is an image - " . $check["mime"] . ".";
                            $uploadOk = 1;
                        } else {
                            echo "File is not an image.";
                            $uploadOk = 0;
                        }
                    }
                    if ($uploadOk == 0) {
                        echo "Sorry, your file was not uploaded.";
                    } else {
                        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                            $pointer = $target_file;
                            echo $pointer;
                        } 
                    }
                ?>
            </tr>
        </table>
         <p><p><p><button class="btn btn-success" type="submit" name="done">เพิ่มสินค้า</button>&nbsp; &nbsp;
        <input class="btn btn-danger" type="button" value='กลับ' onClick="location='index_admin.php'">

    </form>


    <?php
    if (isset($_POST['done'])) {
        $cloth_id = $_POST['cloth_id'];
        $cloth_name = $_POST['cloth_name'];
        $cloth_brand = $_POST['cloth_brand'];
        $cloth_size = $_POST['cloth_size'];
        $cloth_price = $_POST['cloth_price'];
        $cloth_type = $_POST['cloth_type'];
        $cloth_details = $_POST['cloth_details'];
        $image = $_POST['fileToUpload'];

        $sql = "INSERT INTO cloth (cloth_id,name,brand,size,price,type_id,details,cloth_image) values('$cloth_id','$cloth_name','$cloth_brand','$cloth_size','$cloth_price','$cloth_type','$cloth_details','$image')";
        $query = mysqli_query($conn, $sql);
        header("Location:index_admin.php");     
    }
       
    ?>

</body>

</html>